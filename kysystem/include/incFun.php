<?php
	
	
	//判断目录名是否重复
	//$f_fields：字段名
	//$tablename：数据表名
	//$f_str：要查找的字段
	function is_chk($f_fields,$tablename,$f_str){
		$conn = &ADONewConnection('mysql');						//建立mysql连接
		$conn->PConnect("localhost","root","root","db_online");	//连接"db_online"数据库
		$is_chk = true;
		$is_sqlstr = "select $f_fields from $tablename";
		$is_rst = $conn->execute($is_sqlstr);
		while(!$is_rst->EOF){
			if($f_str == $is_rst->fields[0]){
				$is_chk = false;
				break;
			}
			$is_rst->MoveNext();
		}
		return $is_chk;
	}
	//判断文件后缀
	//$f_type：允许文件的后缀类型
	//$f_upfiles：上传文件名
	function f_postfix($f_type,$f_upfiles){
		$is_pass = false;
		$tmp_upfiles = split("\.",$f_upfiles);
		$tmp_num = count($tmp_upfiles);
		for($num = 0; $num < count($f_type);$num++){
			if(strtolower($tmp_upfiles[$tmp_num - 1]) == $f_type[$num]){
				$is_pass = $f_type[$num];
			}
		}
		return $is_pass;
	}
	
	//判断是否是正整数
function check_zzs($varnum){
		 $string_var = "0123456789";
		 $len_string = strlen($varnum);
		 if(substr($varnum,0,1)=="0"){
		  return false;
		  die();
		 }else{ 
		  for($i=0;$i<$len_string;$i++){
		   $checkint = strpos($string_var,substr($varnum,$i,1));
		   if($checkint===false){
			return false;
			die();
		   }
		  }
		 return true;
		 }
}
	
	//脚本起来用
	function jsAlert($msg)
	{
			$getReturn="";
			$getReturn=" <script>alert('".$msg."');history.back(); </script>";
			return $getReturn;
			exit;
	}
	
	//脚本起来用
	function jsWindowLocation($msg,$url)
	{
			$getReturn="";
			$getReturn=" <script>alert('".$msg."');location.href='".$url."'; </script>";
			return $getReturn;
			exit;
	}
	
	
	function arrToStr ($array)  
{  
    // 定义存储所有字符串的数组  
    static $r_arr = array();  
      
    if (is_array($array)) {  
        foreach ($array as $key => $value) {  
            if (is_array($value)) {  
                // 递归遍历  
                arrToStr($value);  
            } else {  
                $r_arr[] = $value;  
            }  
        }  
    } else if (is_string($array)) {  
            $r_arr[] = $array;  
    }  
          
    //数组去重  
    $r_arr = array_unique($r_arr);  
    $string = implode(",", $r_arr);  
      
    return $string;  
} 
	
	
	
function DateDiff($part, $begin, $end)
{
$diff = strtotime($end) - strtotime($begin);
switch($part)
{
case "y": $retval = bcdiv($diff, (60 * 60 * 24 * 365)); break;
case "m": $retval = bcdiv($diff, (60 * 60 * 24 * 30)); break;
case "w": $retval = bcdiv($diff, (60 * 60 * 24 * 7)); break;
case "d": $retval = bcdiv($diff, (60 * 60 * 24)); break;
case "h": $retval = bcdiv($diff, (60 * 60)); break;
case "n": $retval = bcdiv($diff, 60); break;
case "s": $retval = $diff; break;
}
return $retval;
}
function DateAdd($part, $number, $date)
{
$date_array = getdate(strtotime($date));
$hor = $date_array["hours"];
$min = $date_array["minutes"];
$sec = $date_array["seconds"];
$mon = $date_array["mon"];
$day = $date_array["mday"];
$yar = $date_array["year"];
switch($part)
{
case "y": $yar += $number; break;
case "q": $mon += ($number * 3); break;
case "m": $mon += $number; break;
case "w": $day += ($number * 7); break;
case "d": $day += $number; break;
case "h": $hor += $number; break;
case "n": $min += $number; break;
case "s": $sec += $number; break;
}
return date("Y-m-d", mktime($hor, $min, $sec, $mon, $day, $yar));
}

/*
function DateAdd($part, $n, $date)
{
switch($part)
{
case "y": $val = date("Y-m-d H:i:s", strtotime($date ." +$n year")); break;
case "m": $val = date("Y-m-d H:i:s", strtotime($date ." +$n month")); break;
case "w": $val = date("Y-m-d H:i:s", strtotime($date ." +$n week")); break;
case "d": $val = date("Y-m-d H:i:s", strtotime($date ." +$n day")); break;
case "h": $val = date("Y-m-d H:i:s", strtotime($date ." +$n hour")); break;
case "n": $val = date("Y-m-d H:i:s", strtotime($date ." +$n minute")); break;
case "s": $val = date("Y-m-d H:i:s", strtotime($date ." +$n second")); break;
}
return $val;
}
*/



	
	/**  
 * 截取utf-8字符串  
 * @since 2008.12.23  
 * @param string $str 被截取的字符串  
 * @param integer $start 起始位置  
 * @param integer $length 截取长度(每个汉字为3字节)  
 */  
function utf8_strcut($str, $start, $length=null) {   
 preg_match_all('/./us', $str, $match);   
 $chars = is_null($length)? array_slice($match[0], $start ) : array_slice($match[0], $start, $length);   
 
 unset($str);
 
 return implode('', $chars);   
} 


	/**
 * 输出系统信息
 *
 * @param string $oStr
 * @param string $linkURL
 * @param stromg $linkText
 * @param int $refresh
 * @author  
 * @example
 * @see 
 * @copyright www.teamtop.com.cn
 * @version 
 */
function sysMsg($oStr="" ,$linkURL="javascript:history.go(-1)",$linkText="" , $refresh=0) 
{
	global $setting_row,$_Lang_arr;

	if (strlen($oStr) == 0) {
		$oStr = "未知错误";
	}
	if (strlen($linkText) == 0) {
		$linkText = "后退";
	}

	if ($refresh==1) {
		echo "<meta http-equiv=\"refresh\" content=\"3;URL=$linkURL\">";
		$linkText = "自动后退";
	}
	echo '
	<html>
	<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	';
	if ($refresh==1) {
	        echo "<meta http-equiv=\"refresh\" content=\"1;URL=$linkURL\">";
	}
	echo '
	<link rel="stylesheet" href="default.css" type="text/css">
	</head>
	
	<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0">
	<table  border="0" cellspacing="0" cellpadding="0" width="100%">
	        <tr>
	                <td align="center" valign="top">
										<p>&nbsp;</p><p>&nbsp;</p>
	                        <table class="thin" width="500" bordercolor="#000000" cellspacing="0" cellpadding="8" style="border-collapse: collapse" border="1">
	                                <tr>
	                                        <td bgcolor="005BB8"><font color="white"><strong>'.$_Lang_arr['Message_System'].'</strong></font></td>
	                                </tr>
	                                <tr>
	                                        <td style="padding:15" bgcolor="white">
	                                                <p>&nbsp;</p>
	                                                <p align=center><span  style="font: bold 10.5pt; color: red">'.$oStr.'</span>
	                                                <p align=center><a href="'.$linkURL.'">'.$linkText.'</a></p>
	                                                <p>&nbsp;</p>
	                                        </td>
	                                </tr>
	                        </table>
	                        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
	                </td>
	        </tr>
	</table>
	</body>
	</html>';
	exit;
}