<?php

//文件上传

class Upload

{

/*+----------------------------------------------------------------------------------+

*文件上传类：Upload

*制作人：谭宁宁

*版本：1.0

*制作时间：2011-06-09

*最近修改时间：无

*说明：

* 1.该类任何人都可以修改和使用。

* 2.如果你有好的修改建议和方法不妨联系我。:-)

* 3.QQ:597398742    手机：15010836790

* 4.支持开源和分享！

*+----------------------------------------------------------------------------------+

*/

 

/*图片上传类型，只支持三种图片上传，jpg,png,gif

JPG:image/pjpeg;

image/jpeg;

PNG:image/x-png;

image/png;

GIF:image/gif;

ico:image/x-icon;

*/

//是否需要上传

public $onoff=false;

//上传文件最大值

public $maxsize='';

//上传文件路径

public $uppath='';

//错误代号

public $error=0;

//错误信息

public $error_info;

//检查文件格式信息

public $checkinfo='';

//方法反馈信息

public $reinfo='';

//上传的文件

public $upfile="";

//其他信息变量

 

//图片上传方法带参数 Up_Images(开关，上传的文件，上传路径，上传类型，最大大小)

/*+----------------------------------------------------------------------------------+

*|图片上传方法名：Up_Images()

*|参数传递5：

*|$onoff->开关。————判断是否需要上传图片！true:开启， false:关闭

*|$upfile->上传的文件。————从表单获取到的文件数组

*|$uppath->上传的路径。————上传文件的存放路径！（需自己设定）

*|$file_type->上传文件类型。————要上传的文件类型判断。（区分图片、普通文件txt,word,pdf...、音频、视频。目前只支持图片）

*|$maxsize->最大大小限制。————文件大小的判断。该值由php.ini文件决定。用户需通过ini_get('file_uploads') ? ini_get('upload_max_filesize') : 'Disabled'来获取最大上传大小

*+----------------------------------------------------------------------------------+

*/

public function Up_Images($onoff,$upfile,$uppath,$file_type,$maxsize)

{

if($onoff)

{

$check=Upload::CheckFileType($upfile,$file_type,$maxsize);

if($check=='ok')

{

$postfix=array_reverse(explode(".",$upfile['name']));  //获取文件后缀名

$savepath=$uppath.date("Ymd_His",time()).".".$postfix[0];//设置文件上传的地址




if(move_uploaded_file($upfile['tmp_name'],$savepath))

{

$check=$savepath;

}

else

{

$check=Upload::Error(99);

}

}

}

return $check;

}




//判断上传的文件类型及文件的大小 带参数CheckFileType(需验证的文件,文件类型,大小验证)

/*+----------------------------------------------------------------------------------+

*|文件格式&大小判断方法名：CheckFileType()，该方法通过Up_Image()方法来调用

*|如果通过验证，则返回 ok，否则，通过调用Error()方法返回一段错误提示语句

*|参数传递5：

*|$upfile->需要判断的文件。————进行判断的文件

*|$file_type->文件类型。————要上传的文件类型判断。（区分图片、普通文件txt,word,pdf...、音频、视频。目前只支持图片）

*|$maxsize->最大大小限制。————文件大小的判断。该值由php.ini文件决定。用户需通过ini_get('file_uploads') ? ini_get('upload_max_filesize') : 'Disabled'来获取最大上传大小

*+----------------------------------------------------------------------------------+

*/

public function CheckFileType($upfile,$file_type,$maxsize)

{

$checkinfo=0;

switch ($file_type)

{

case 'images':

//图片格式和大小判断

if($upfile['type']!='image/pjpeg' && $upfile['type']!='image/jpeg' && $upfile['type']!='image/png' && $upfile['type']!='image/x-png' && $upfile['type']!='image/gif')

{ $checkinfo=Upload::Error(101); break; }

else if($upfile['size']>$maxsize*1024*1024)

{ $checkinfo=Upload::Error(102); break; }

else

{ $checkinfo='ok'; break; }

break;

default:

$checkinfo=Upload::Error(100);

break;

}

return $checkinfo;

}

 

//错误提示信息反馈 带参数 Error(错误代号)

/*+----------------------------------------------------------------------------------+

*|错误信息名：CheckFileType()，该方法通过可以被Upload类下的任意方法来调用，返回一段错误提示！

*|每句错误提示需包含“错误代号”四字，以方便前台判断是否提交成功。这么做是因为个人技术和思想问题导致。

*|参数传递1：

*|$upfile->$error。————错误代码。

*+----------------------------------------------------------------------------------+

*/

public function Error($error)

{

$error_info='';

switch ($error)

{

case 101:

$error_info="图片格式不正确！/n只支持jpg、gif、png三种格式图片 /n错误代号:101";

break;

case 102:

$error_info="上传文件太大/n最大大小：".$maxsize." /n错误代号:102:";

break;

case 100:

$error_info="当前上传文件损坏或无法识别，您需要重新选择上传文件！ /n错误代号:100:";

break;

case 99:

$error_info="无法将文件上传到指定路径！/n解决方案：/n1：请检查该路径是否存在/n2：检查该路劲是否具备读写权限！ /n错误代号:99:";

default:

$error_info="未知的错误！/n错误代号：未知";

break;

}

return $error_info;

}

}

?>
