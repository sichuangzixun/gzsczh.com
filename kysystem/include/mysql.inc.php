<?php
error_reporting(7);
// db class for mysql
// this class is used in all scripts
// do NOT fiddle unless you know what you are doing

class DB_Sql {
  var $database = "";

  var $link_id  = 0;
  var $query_id = 0;
  var $record   = array();

  var $errdesc    = "";
  var $errno   = 0;
  var $reporterror = 1;

  var $version = '';

  var $server   = "localhost";
  var $user     = "root";
  var $password = "root";

  function connect() {
    global $usepconnect;
    // connect to db server

    if ( 0 == $this->link_id ) {
      if ($this->password=="") {
        if ($usepconnect==1) {
          $this->link_id=mysql_pconnect($this->server,$this->user);
        } else {
          $this->link_id=mysql_connect($this->server,$this->user);
        }
      } else {
        if ($usepconnect==1) {
          $this->link_id=mysql_pconnect($this->server,$this->user,$this->password);
        } else {
          $this->link_id=mysql_connect($this->server,$this->user,$this->password);
        }
      }
      if (!$this->link_id) {
        $this->halt("Link-ID == false, connect failed");
      }
      if ($this->database!="") {
        if(!mysql_select_db($this->database, $this->link_id)) {
          $this->halt("cannot use database ".$this->database);
        }
		mysql_query("SET NAMES utf8");
      }
    }
  }

  function geterrdesc() {
    $this->error=mysql_error();
    return $this->error;
  }

  function geterrno() {
    $this->errno=mysql_errno();
    return $this->errno;
  }

  function select_db($database="") {
    // select database
    if ($database!="") {
      $this->database=$database;
    }

    if(!mysql_select_db($this->database, $this->link_id)) {
      $this->halt("cannot use database ".$this->database);
    }

  }

  function query($query_string) {
    global $query_count,$showqueries,$explain,$querytime;
    // do query

    if ($showqueries) {
      echo "Query: $query_string\n";

      global $pagestarttime;
      $pageendtime=microtime();
      $starttime=explode(" ",$pagestarttime);
      $endtime=explode(" ",$pageendtime);

      $beforetime=$endtime[0]-$starttime[0]+$endtime[1]-$starttime[1];

      echo "Time before: $beforetime\n";
    }

    $this->query_id = mysql_query($query_string,$this->link_id);
    if (!$this->query_id) {
      $this->halt("Invalid SQL: ".$query_string);
    }

    $query_count++;

    if ($showqueries) {
      $pageendtime=microtime();
      $starttime=explode(" ",$pagestarttime);
      $endtime=explode(" ",$pageendtime);

      $aftertime=$endtime[0]-$starttime[0]+$endtime[1]-$starttime[1];
      $querytime+=$aftertime-$beforetime;

      echo "Time after:  $aftertime\n";

      if ($explain and substr(trim(strtoupper($query_string)),0,6)=="SELECT") {
        $explain_id = mysql_query("EXPLAIN $query_string",$this->link_id);
        echo "</pre>\n";
        echo "
        <table width=100% border=1 cellpadding=2 cellspacing=1>
        <tr>
          <td><b>table</b></td>
          <td><b>type</b></td>
          <td><b>possible_keys</b></td>
          <td><b>key</b></td>
          <td><b>key_len</b></td>
          <td><b>ref</b></td>
          <td><b>rows</b></td>
          <td><b>Extra</b></td>
        </tr>\n";
        while($array=mysql_fetch_array($explain_id)) {
          echo "
          <tr>
            <td>$array[table]&nbsp;</td>
            <td>$array[type]&nbsp;</td>
            <td>$array[possible_keys]&nbsp;</td>
            <td>$array[key]&nbsp;</td>
            <td>$array[key_len]&nbsp;</td>
            <td>$array[ref]&nbsp;</td>
            <td>$array[rows]&nbsp;</td>
            <td>$array[Extra]&nbsp;</td>
          </tr>\n";
        }
        echo "</table>\n<BR><hr>\n";
        echo "\n<pre>";
      } else {
        echo "\n<hr>\n\n";
      }
    }

    return $this->query_id;
  }

  function fetch_array($query_id=-1,$query_string="") {
    // retrieve row
    if ($query_id!=-1) {
      $this->query_id=$query_id;
    }
    if ( isset($this->query_id) ) {
      $this->record = @mysql_fetch_array($this->query_id);
    } else {
      if ( !empty($query_string) ) {
        $this->halt("Invalid query id (".$this->query_id.") on this query: $query_string");
      } else {
        $this->halt("Invalid query id ".$this->query_id." specified");
      }
    }

    return $this->record;
  }
  /*
  *获取服务器的版本
  *
  */
  function version() {
		if(empty($this->version)) {
			$this->version = mysql_get_server_info($this->link_id);
		}
		return $this->version;
  }
  /*
  *获取mysql错误
  *
  */
  function error() {
		return (($this->link) ? mysql_error($this->link) : mysql_error());
  }
  /*
  *获取mysql错误编号
  *
  */
  function errno() {
		return intval(($this->link) ? mysql_errno($this->link) : mysql_errno());
  }
  /*
  *获取结果集数据返回数字数组
  *@param string 结果集标识
  */
  function fetch_row($query) {
		$query = mysql_fetch_row($query);
		return $query;
	}

  function free_result($query_id=-1) {
    // retrieve row
    if ($query_id!=-1) {
      $this->query_id=$query_id;
    }
    return @mysql_free_result($this->query_id);
  }

  function query_first($query_string) {
    // does a query and returns first row
    $query_id = $this->query($query_string);
    $returnarray=$this->fetch_array($query_id, $query_string);
    $this->free_result($query_id);
    return $returnarray;
  }

  function data_seek($pos,$query_id=-1) {
    // goes to row $pos
    if ($query_id!=-1) {
      $this->query_id=$query_id;
    }
    return mysql_data_seek($this->query_id, $pos);
  }

  function num_rows($query_id=-1) {
    // 返回记录数量
    if ($query_id!=-1) {
      $this->query_id=$query_id;
    }
    return mysql_num_rows($this->query_id);
  }

  function num_fields($query_id=-1) {
    // 返回字段数量
    if ($query_id!=-1) {
      $this->query_id=$query_id;
    }
    return mysql_num_fields($this->query_id);
  }

  function field_name($query_id=-1,$n=0) {
    // 返回字段名称
    if ($query_id!=-1) {
      $this->query_id=$query_id;
    }
    return mysql_field_name($this->query_id,$n);
  }

  function insert_id() {
    // returns last auto_increment field number assigned

    return mysql_insert_id($this->link_id);

  }
  
  function close() {
  	// closes connection to the database
	
		return mysql_close();
  }

  function halt($msg) 
  {
    $this->errdesc = mysql_error();
    $this->errno = mysql_errno();
    // prints warning message when there is an error
    global $technicalemail, $bbuserinfo, $scriptpath;

    if ($this->reporterror==1) {
      $message="<b>Database error in:</b> $this->appname $GLOBALS[templateversion]:\n\n$msg<br><br>\n";
      $message.="<b>mysql error code:</b>: <font color=red>$this->errdesc</font><br>\n\n";
      $message.="<b>mysql error number:</b>$this->errno<br>\n\n";
      $message.="<b>Scripts:</b>" . (($scriptpath) ? $scriptpath : getenv("REQUEST_URI")) . "<br>\n";
      $message.="<b>Referer URL:</b>".getenv("HTTP_REFERER")."<br>\n";
	  $message.="<b>Date:</b>".date("l dS of F Y h:i:s A")."<br>\n";

      if ($technicalemail) {
        @mail ($technicalemail,"$this->appshortname Database error!",$message,"From: $technicalemail");
      }

      echo "<html><head><title> Database Error</title><style>P,BODY{FONT-FAMILY:tahoma,arial,宋体;FONT-SIZE:12px;}</style><body>\n\n \n\n";

      echo "<blockquote><p>&nbsp;</p><p><b> 数据库好象发生了一些微小的错误.</b><br>\n";
      echo "请按浏览器的 <a href=\"javascript:window.location=window.location;\">刷新</a> 按钮重试.</p>";
      echo "如果问题仍然存在, 你也可以与我们的<a href=\"mailto:$technicalemail\">技术支持</a>联系.</p>";
      echo "<p>我们为由此给你带来不便深感抱歉.</p>";
		echo $message ;
		  echo "</blockquote></body></head></html>";
      exit;
    }
  }
  
     // SQL function to MySQL databases

   // select SQL function
   // INPUT: $sql, the SQL statement submitted.
   // RETURN: the selected results in an array OR false
   function select($sql="")
   {
      if (empty($sql)) return false;
      if (empty($this->link_id)) return false;

      $conn = $this->link_id;
      $results = mysql_query($sql,$conn);
      if ((!$results) or (empty($results))) {
         //mysql_free_result($results);
         return false;
      }
      $count = 0;
      $data = array();
      while ($row = mysql_fetch_array($results)) {
         $data[$count] = $row;
         $count++;
      }
      mysql_free_result($results);
      return $data;
   }
  
}


?>