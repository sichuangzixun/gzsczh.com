<?php    

 class SubPages{    

       

   private  $each_disNums;//每页显示的条目数    

   private  $nums;//总条目数    

   private  $current_page;//当前被选中的页    

  private  $sub_pages;//每次显示的页数    

   private  $pageNums;//总页数    

   private  $page_array = array();//用来构造分页的数组    

  private  $subPage_link;//每个分页的链接    

   private  $subPage_type;//显示分页的类型    

   /*   

013    __construct是SubPages的构造函数，用来在创建类的时候自动运行.   

014    @$each_disNums   每页显示的条目数   

015    @nums     总条目数   

016    @current_num     当前被选中的页   

017    @sub_pages       每次显示的页数   

018    @subPage_link    每个分页的链接   

019    @subPage_type    显示分页的类型   

020       

021    当@subPage_type=1的时候为普通分页模式   

022          example：   共4523条记录,每页显示10条,当前第1/453页 [首页] [上页] [下页] [尾页]   

023          当@subPage_type=2的时候为经典分页样式   

024          example：   当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页]   

025    */   

   function __construct($each_disNums,$nums,$current_page,$sub_pages,$subPage_link,$subPage_type){    

    $this->each_disNums=intval($each_disNums);    

    $this->nums=intval($nums);    

    if(!$current_page){    

     $this->current_page=1;    

     }else{    

     $this->current_page=intval($current_page);    

     }    

    $this->sub_pages=intval($sub_pages);    

    $this->pageNums=ceil($nums/$each_disNums);    

    $this->subPage_link=$subPage_link;     

    $this->show_SubPages($subPage_type);     

    //echo $this->pageNums."--".$this->sub_pages;    

   }    

        

       

   /*   

043     __destruct析构函数，当类不在使用的时候调用，该函数用来释放资源。   

044    */   

   function __destruct(){    

     unset($each_disNums);    

     unset($nums);    

    unset($current_page);    

    unset($sub_pages);    

     unset($pageNums);    

     unset($page_array);    

     unset($subPage_link);    

    unset($subPage_type);    

    }    

        

   /*   

057     show_SubPages函数用在构造函数里面。而且用来判断显示什么样子的分页     

058    */   

   function show_SubPages($subPage_type){    

     if($subPage_type == 1){    

     $this->subPageCss1();    

     }elseif ($subPage_type == 2){    

    $this->subPageCss2();    

     }    

   }    

        

       

   /*   

069     用来给建立分页的数组初始化的函数。   

070    */   

   function initArray(){    

     for($i=0;$i<$this->sub_pages;$i++){    

     $this->page_array[$i]=$i;    

     }    

     return $this->page_array;    

    }    

        

        

   /*   

080     construct_num_Page该函数使用来构造显示的条目   

081     即使：[1][2][3][4][5][6][7][8][9][10]   

082    */   

   function construct_num_Page(){    

     if($this->pageNums < $this->sub_pages){    

     $current_array=array();    

      for($i=0;$i<$this->pageNums;$i++){     

      $current_array[$i]=$i+1;    

      }    

     }else{    

     $current_array=$this->initArray();    

      if($this->current_page <= 3){    

       for($i=0;$i<count($current_array);$i++){    

       $current_array[$i]=$i+1;    

       }    

      }elseif ($this->current_page <= $this->pageNums && $this->current_page > $this->pageNums - $this->sub_pages + 1 ){    

       for($i=0;$i<count($current_array);$i++){    

       $current_array[$i]=($this->pageNums)-($this->sub_pages)+1+$i;    

       }    

      }else{    

       for($i=0;$i<count($current_array);$i++){    

       $current_array[$i]=$this->current_page-2+$i;    

       }    

      }    

     }    

         

     return $current_array;    

    }    

        

   /*   

110    构造普通模式的分页   

111    共4523条记录,每页显示10条,当前第1/453页 [首页] [上页] [下页] [尾页]   

112    */   

   function subPageCss1(){    

    $subPageCss1Str="";    

    $subPageCss1Str.="共".$this->nums."条记录，";    

    $subPageCss1Str.="每页显示".$this->each_disNums."条，";    

    $subPageCss1Str.="当前第".$this->current_page."/".$this->pageNums."页 ";    

     if($this->current_page > 1){    

     $firstPageUrl=$this->subPage_link."1";    

     $prewPageUrl=$this->subPage_link.($this->current_page-1);    

     $subPageCss1Str.="[<a href='$firstPageUrl'>首页</a>] ";    

     $subPageCss1Str.="[<a href='$prewPageUrl'>上一页</a>] ";    

     }else {    

     $subPageCss1Str.="[首页] ";    

     $subPageCss1Str.="[上一页] ";    

     }    

         

     if($this->current_page < $this->pageNums){    

     $lastPageUrl=$this->subPage_link.$this->pageNums;    

     $nextPageUrl=$this->subPage_link.($this->current_page+1);    

     $subPageCss1Str.=" [<a href='$nextPageUrl'>下一页</a>] ";    

     $subPageCss1Str.="[<a href='$lastPageUrl'>尾页</a>] ";    

     }else {    

     $subPageCss1Str.="[下一页] ";    

     $subPageCss1Str.="[尾页] ";    

     }    

         

     echo $subPageCss1Str;    

        

    }    

   /*   

144    构造经典模式的分页   

145    当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页]   

146    */   

   function subPageCss2(){    

    $subPageCss2Str="";    

    $subPageCss2Str.="No.<font color=red>".$this->current_page."</font>/".$this->pageNums."Page,Total <font color=red>".$this->nums."</font> item(s)  &nbsp;";    

         

         

     if($this->current_page > 1){    

     $firstPageUrl=$this->subPage_link."1";    

	if(QSX_SYSTEM_HTML=="true")
	{
		$prewPageUrl=$this->subPage_link.($this->current_page-1);    
		$prewPageUrl=$prewPageUrl."html";
	}
	else
	{
		$prewPageUrl=$this->subPage_link.($this->current_page-1);   
		
	}
     

     $subPageCss2Str.="<font color=gray>&nbsp;<a href='$prewPageUrl'>&laquo;</a>&nbsp;</font>";    

     }else {    

     

     $subPageCss2Str.="<font color=gray>&nbsp;&laquo;&nbsp;</font> ";    

     }    

         

    $a=$this->construct_num_Page();    

     for($i=0;$i<count($a);$i++){    

     $s=$a[$i];    

      if($s == $this->current_page ){    

     $subPageCss2Str.="<a style='background:#ccc;'>".$s."</a> ";    

      }else{    

	if(QSX_SYSTEM_HTML=="true")
	{
		 $url=$this->subPage_link.$s;  
		 $url=$url.".html";  
	}
	else
	{
	    $url=$this->subPage_link.$s;    
	}
     

      $subPageCss2Str.="<a href='$url'>".$s."</a> ";    

      }    

     }    

         

     if($this->current_page < $this->pageNums){    

    $lastPageUrl=$this->subPage_link.$this->pageNums;    


if(QSX_SYSTEM_HTML=="true")
	{
		  $nextPageUrl=$this->subPage_link.($this->current_page+1);    
		  $nextPageUrl=$nextPageUrl.".html";  
	}
	else
	{
	     $nextPageUrl=$this->subPage_link.($this->current_page+1);    
	}


    

     $subPageCss2Str.="<a style='border:0;background:url();' href='$nextPageUrl'>&nbsp;&raquo;&nbsp;</a>&nbsp; ";    

     

     }else {    

     $subPageCss2Str.="&nbsp;&raquo;&nbsp; ";    

     

     }    

     echo $subPageCss2Str;    
?>

	<script>function jump(numi){numi2=numi;
            if(numi2==1)numi2="1";
            var urls=window.location.href;
            urls=urls.substring(urls.indexOf("")+5,urls.length);			
            var num1=urls.lastIndexOf("/");		
			
			<?php if(QSX_SYSTEM_HTML=="true") {  ?>	
			
            if(num1!= -1)location="<?php echo $this->subPage_link?>"+numi2+".html";
            else location="<?php echo $this->subPage_link?>"+numi2+".html";			
            }
			
			<?php 
			} 
				else
			{
			?>
			 if(num1!= -1)location="<?php echo $this->subPage_link?>"+numi2;
            else location="<?php echo $this->subPage_link?>"+numi2;			
            }
			<?php 
				}
			?>
			</script> 

		<select name="page" style="font-size:10px;" onchange="jump(this.value)">
		<script>for (i=1;i<=<?php echo $this->pageNums;?>;i++)
		{document.write("<option");
		if(<?php echo $this->current_page;?>==i)document.write(" selected")
		document.write(" value="+i+">PAGE "+i+"</option>");}
		</script>
		</select>


<?php 
    }    

 }    

 ?> 
