<?php


	include "../../include/check.php";
	include "../../include/db.php";
	include "../../include/incFun.php";
	include "../../../admin/ky_ComInfoView.php";
	include "../../include/config.php";
	include "../../include/makehtml.php";
	include "../../include/historyHead.php";
	include "../../include/mysql.inc.php";
	include "../../include/page.php";	
	
	

	$db = new DB_Sql;
	$db-> server = $dbServer;
	$db-> user	= $dbUser;
	$db-> password = $dbPassword;
	$db-> database = $dbName;
	$db-> connect();
	
	
	
	//echo dirname(__FILE__);
	//echo dirname(dirname(__FILE__));
	
	
	function getSelectTable($conn)
{
		$tSql="select * From  ky_targettype Order by nSort Desc";
		$tRs = mysql_query($tSql,$conn);
	
		while($row = mysql_fetch_array($tRs))
		{
				
						$tReturn=$tReturn."<option value='".$row[nId]."'>".$row[targetName]."</option>";
				
				
		}

		return $tReturn;

}

	$channelID=$_GET[channelID];
	if(empty($channelID))
	{
		$channelID=$_POST[channelID];	
	}
    $topicID=$_GET[topicID];
	if(empty($topicID))
	{
		$topicID=$_POST[topicID];	
	}
	if(!empty($channelID))
	{
		$nType=getTableName("ky_channel","channelTitle","channelID",$channelID);
		$nType1=getTableName("ky_topic","topicTitle","topicID",$topicID);
	}
	
	$getSql="channelID=".$channelID."&topicID=".$topicID."&nType=".$nType."&nType1=".$nType1;
	
	
/**
 * 显示 频道的相关栏目
 *
 * @param int $channelID
 * @param int $topicID
 * @param string $FunctionName
 * @param int $MoveID 轉移版本
 * @return string
 * @author  danny
 * @example
 * @see 
 * @copyright www.teamtop.com.cn
 * @version 2007-06-30
 */ 
function FunctionTopicList($class=0,$channelID=0,$topicID,$FunctionName,$MoveID="") {

	global $db,$prefixion;
		

		//if($channelID>0)$sqlwhere.=" AND channelID=".$channelID;
		//搜索出所有的频道并循环
		$channel_sql = "SELECT * FROM ky_channel  ORDER BY channelOrderBy ASC";
		$channel_query = $db->query($channel_sql);
		while($channel_result = $db->fetch_array($channel_query)){
			if($channelID == $channel_result[channelID]){
			echo "<option value=\"0,".$channel_result[channelID]."\" selected>|-&nbsp;".$channel_result[channelTitle]."</option>";
			}
			else{
			echo "<option value=\"0,".$channel_result[channelID]."\">|-&nbsp;".$channel_result[channelTitle]."</option>";
			}
			getTopicList(1,$channel_result[channelID],$topicID,0,$FunctionName);//递归调用子栏目
		}
		
}

/**
 * 子函数,处理栏目列表
 *
 * @param int $topicClass
 * @param int $channelID
 * @param int $topicID
 * @param int $subTopicID
 * @param string $FunctionName
 * @param string $strlist
 * @author  danny
 * @example
 * @see 
 * @copyright www.teamtop.com.cn
 * @version 2007-06-30
 */
function getTopicList($topicClass=1,$channelID,$topicID=0,$subTopicID=0,$FunctionName){
	global $db,$prefixion;
			//如果有指定栏目--------------------
			if($subTopicID>0){
				$sqlwhere = "AND topicParentID=".$subTopicID;
			}else{
				$sqlwhere = "AND topicParentID=0";
			}

			if($FunctionName!=""){
				$sqlwhere .= " AND functionName='".$FunctionName."' ";
			}
			
			$sql = "SELECT topicID,channelID,topicTitle,topicImage,functionName,topicOrderBy,isPublic FROM  ky_topic WHERE channelID=".$channelID." $sqlwhere ORDER BY topicOrderBy ASC";

			
			//搜索并显示栏目列表==================================
			$topic_result = $db->query($sql);
	
			//生成空格
			$tmp = '&nbsp;&nbsp;';
			for($i=0;$i<$topicClass;$i++){
				$tmp .= '&nbsp;&nbsp;';
			}
			$tmp .= '|-';
	
			while ($topic_row = $db->fetch_array($topic_result)) {
				if ($topicID == $topic_row [topicID]) {
					echo "<option value=".$topic_row [topicID]." selected>".$tmp."&nbsp;".$topic_row [topicTitle]."</option>";
				} else {
					echo "<option value=".$topic_row [topicID].">".$tmp."&nbsp;".$topic_row [topicTitle]."</option>";
				}
				//调用自身
				getTopicList($topicClass+1,$channelID,$topicID,$topic_row['topicID'],$FunctionName);
			}
			
}



//部门管理  getTopicList(1,$channel_result[channelID],$topicID,0,$FunctionName);//递归调用子栏目
function getBuMen($topicClass=1,$channelID,$topicID=0,$subTopicID=0,$FunctionName){
	global $db,$prefixion;
			//如果有指定栏目--------------------
			if($subTopicID>0){
				$sqlwhere = "AND topicParentID=".$subTopicID;
			}else{
				$sqlwhere = "AND topicParentID=0";
			}

			if($FunctionName!=""){
				$sqlwhere .= " AND functionName='".$FunctionName."' ";
			}
			
			$sql = "SELECT topicID,channelID,topicTitle,topicImage,functionName,topicOrderBy,isPublic FROM   ky_department WHERE topicID<>0 $sqlwhere ORDER BY topicOrderBy ASC";

			
			//搜索并显示栏目列表==================================
			$topic_result = $db->query($sql);
	
			//生成空格
			$tmp = '&nbsp;&nbsp;';
			for($i=0;$i<$topicClass;$i++){
				$tmp .= '&nbsp;&nbsp;';
			}
			$tmp .= '|-';
	
			while ($topic_row = $db->fetch_array($topic_result)) {
				if ($topicID == $topic_row [topicID]) {
					echo "<option value=".$topic_row [topicID]." selected>".$tmp."&nbsp;".$topic_row [topicTitle]."</option>";
				} else {
					echo "<option value=".$topic_row [topicID].">".$tmp."&nbsp;".$topic_row [topicTitle]."</option>";
				}
				//调用自身
				getBuMen($topicClass+1,$channelID,$topicID,$topic_row['topicID'],$FunctionName);
			}
			
}

?>