<?php
	session_start();
	include "include/check.php";
	include "include/db.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>汉晨商务网站管理系统</title>
<style type="text/css">
body { margin:0; padding:0; border:0; background:#fff; }
div#www48cn { padding:9px; overflow:hidden; zoom:1; background:#EFF8FE; border-bottom:solid 1px #26A8E0; font:normal normal 12px/22px Tahoma, Arial, \5b8b\4f53; }
table.toptable { width:100%; border:1px solid #B3CDE8; background:#fff; line-height:20px; }
dl,dl * { margin:0; padding:0; list-style:none; border:0; overflow:hidden; font:normal normal 12px Tahoma, Arial, \5b8b\4f53; padding:6px 0; }
dt { float:left; padding:0 9px; }
dd { float:right; padding:0 9px; }
</style>
</head>

<body>
<div id="www48cn">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table id="service" align="center" cellpadding="4" cellspacing="1" border="0" class="toptable">
                    <tr>
                        <td colspan="2" background="img/r_t.gif" style="font-weight: bold">后台管理首页</td>
                    </tr>
                </table>
<p class="welcome clearfix" style="padding:10px">
系统版本：汉晨商务网站管理系统V2.0<br />
开发公司：广州汉晨科技信息有限公司<br /> 
技术支持：<a href="http://www.48cn.com" target="_blank">汉晨技术中心</a>
</p>
                <table border="0" cellpadding="0" align="center" cellspacing="0" width="100%">
                    <tr>
                        <td style="padding: 10px; color: #0F5B90">
<p>&nbsp;</p>
<div id="sysinfo" style="background-color:#EFF8FE;">
<fieldset>
	<legend>系统信息</legend>
	<p class="welcomep" style="padding:10px;">
	
	    登录账号:<?php echo $_SESSION[admin_name];?> <br />
		服务器名称:<?php echo $_SERVER['SERVER_NAME']; ?> <br />
		浏览器信息:<?php echo $_SERVER['HTTP_USER_AGENT']; ?> <br />
		当前编码:<?php echo $_SERVER['HTTP_ACCEPT_CHARSET']; ?> <br />
		头信息内容:<?php echo $_SERVER['HTTP_HOST']; ?> <br />
		
		
		当前IP:<?php echo $_SERVER['REMOTE_ADDR']; ?> <br />
		当前时间:<?php echo date("Y.m.d"); ?> <br />
		
	</p>

</fieldset>
</div>
                        </td>
                    </tr>
                </table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
            </td>
            <td>
            </td>
        </tr>
    </table>
</div>
<dl>
	<dt>&copy; 2010 <a href="http://www.48cn.com" target="_blank">48cn.com</a> All Rights Reserved.</dt>
	<dd>
		<a href="http://www.48cn.com" target="_blank">汉晨主页</a>
		│
		<a href="http://bbs.48cn.com" target="_blank">技术支持</a>
		│
		<a href="http://www.48cn.com/product.html" target="_blank">产品中心</a>
		│
		<a href="http://www.48cn.com/contact.html" target="_blank">联系方式</a>
	</dd>
</dl>
</body>
</html>