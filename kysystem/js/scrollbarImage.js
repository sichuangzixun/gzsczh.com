//Modify By PS 2005-07-28 winpzs@hotmail.com(Build By griefforyou Email:griefforyou@163.com)
//

//============请将设置部分,放置到你的网页上==================================
/*
<Script language="JavaScript1.2" type="text/javascript">
<!--
	var ImgArr,LinkArr;				//图片文件名数组/链接名数组
	var Text=[];					//Tip说明
	var ImgWidth , ImgHeight;	//'每个图片宽/高/每次显示图片数
	var ScrollBarWidth,ScrollBarHeight;
	var DX , Delay;						//'每次移动像素个数/每次移动时间间隔(毫秒)
	var psLR;	//1为左,2为右,3为上,4为下
	var psgStop;//鼠标经过是否停止
	psLR = 1;		//1为左,2为右
	psgStop = true;	//鼠标经过是否停止(true为停止,false不停止)
	ImgWidth=100;	//'每个图片宽
	ImgHeight=60;	//'每个图片高
	ScrollBarWidth = 650;		//总长度
	ScrollBarHeight = ImgHeight;//总宽度
	DX=2;			//'步(速度)越大越快
	Delay=10;		//'速度越小越快

	//'定义图片集
	ImgArr = new Array("./2.jpg",
		"./11.jpg",
		"./13.jpg",
		"./3.jpg");

	//'定义链接集(请与图片集个数相等)
	LinkArr = new Array("./2.jpg",
		"./11.jpg",
		"./13.jpg",
		"./3.jpg");

	//定义Tip说明(请与图片集个数相等)
	Text = new Array(["标题1","图片1,你好吗?"],
		["标题2","图片2,我叫PS."],
		["标题3","图片3,你在哪里?"],
		["","图片4,你还好吗?"]);

-->
</Script>
<Script Language="JavaScript1.2" src="./scrollbarImage.js"></Script>
*/
//======以上为设置部分=======================================================

//===================初始化===放置到你的网页上================
//	<body Onload="JavaScript:Init();">
//===================初始化===================================

//===================实体部分===放置到你的网页上================
//	<Script language="JavaScript">priImageDiv();</Script>
//===================实体部分===================================

//==================以下是代码部分,请完善=====================
var img;							//'图片对像(用于预载图片)
var Str , First;				//'字符串/最左边的图片索引/最后一个图片文件索引
var psMoveIN;
	psMoveIN = false;	//暂停标志,请不要设置
	First=0;

	var psgnArrLen,psgnArrLenTemp;
	if (ImgArr.length <= LinkArr.length) psgnArrLen = ImgArr.length; else psgnArrLen = LinkArr.length;
	psgnArrLenTemp = psgnArrLen;
//	if (psgnArrLen < 4) psgnArrLen = parseInt(4/psgnArrLen) * psgnArrLen + psgnArrLen;

	var psnTemp1;
	if (psLR <= 2)
		psnTemp1 = parseInt(ScrollBarWidth/ImgWidth) + 1;
	else
		psnTemp1 = parseInt(ScrollBarHeight/ImgHeight) + 1;
	if (psnTemp1 > psgnArrLen)
	{
		var ifn;
		if (psnTemp1%psgnArrLen != 0)
		{
			psgnArrLen = parseInt(psnTemp1/psgnArrLen)*psgnArrLen+psgnArrLen;
		}
		else
		{
			psgnArrLen = psnTemp1 + psgnArrLen;
		}
	}

	//window.document.write(psgnArrLen);

	function gotoURL(URL,Target)
	{
		window.open(URL,Target);
	}

	//'预载图片
	var i,j;
	function Init()
	{
		applyCssFilter();	//Tips初始化

		Str = new String;
		if (psLR==1)
		{
			for (i=0;i<psgnArrLen;i++)
			{
				j = i;
				if (j >= psgnArrLenTemp) j = i % psgnArrLenTemp;
				Str += "<img src=\'" + ImgArr[j] + "\' width=\'" + ImgWidth + "\' height=\'" + ImgHeight + "\' border=\'0\' id=\'img" + i + "\' onclick=\"JavaScript:gotoURL(\'" + LinkArr[j] + "\',\'_blank\')\" onMouseOut=\"htm()\" onMouseOver=\"stm(Text[" + j + "],Style[7])\" style=\'position:absolute;left:" + ((ImgWidth+5)*i) + "\'>";
			}
		}
		else if (psLR==2)
		{
			for (i=0;i<psgnArrLen;i++)
			{
				j = i;
				if (j >= psgnArrLenTemp) j = i % psgnArrLenTemp;
				Str += "<img src=\'" + ImgArr[j] + "\' width=\'" + ImgWidth + "\' height=\'" + ImgHeight + "\' border=\'0\' id=\'img" + i + "\' onclick=\"JavaScript:gotoURL(\'" + LinkArr[j] + "\',\'_blank\')\" onMouseOut=\"htm()\" onMouseOver=\"stm(Text[" + j + "],Style[7])\" style=\'position:absolute;left:" + (ScrollBarWidth/2-(ImgWidth+5)*i) + "\'>";
			}
		}
		else if (psLR==3)
		{
			for (i=0;i<psgnArrLen;i++)
			{
				j = i;
				if (j >= psgnArrLenTemp) j = i % psgnArrLenTemp;
				Str += "<img src=\'" + ImgArr[j] + "\' width=\'" + ImgWidth + "\' height=\'" + ImgHeight + "\' border=\'0\' id=\'img" + i + "\' onclick=\"JavaScript:gotoURL(\'" + LinkArr[j] + "\',\'_blank\')\" onMouseOut=\"htm()\" onMouseOver=\"stm(Text[" + j + "],Style[7])\" style=\'position:absolute;left:" + (-ScrollBarWidth/2) + ";top:" + ((ImgHeight+5)*i) + "\'>";
			}
		}
		else if (psLR==4)
		{
			for (i=0;i<psgnArrLen;i++)
			{
				j = i;
				if (j >= psgnArrLenTemp) j = i % psgnArrLenTemp;
				Str += "<img src=\'" + ImgArr[j] + "\' width=\'" + ImgWidth + "\' height=\'" + ImgHeight + "\' border=\'0\' id=\'img" + i + "\' onclick=\"JavaScript:gotoURL(\'" + LinkArr[j] + "\',\'_blank\')\" onMouseOut=\"htm()\" onMouseOver=\"stm(Text[" + j + "],Style[7])\" style=\'position:absolute;left:" + (-ScrollBarWidth/2) + ";top:" + (ScrollBarHeight/2-(ImgHeight+5)*i) + "\'>";
			}
			//alert(Str);
		}

		pssl1.innerHTML=Str;
		pssl1.style.width = (ScrollBarWidth + 15) + "px";
		pssl1.style.height = ScrollBarHeight + "px";
		tk.style.width = (ScrollBarWidth) + "px";
		tk.style.height = ScrollBarHeight + "px";
		ScrollIt();
	}
	//'滚动
	function ScrollIt()
	{
		switch(psLR)
		{
			case 1:
				ScrollItL();
				break;
			case 2:
				ScrollItR();
				break;
			case 3:
				ScrollItU();
				break;
			case 4:
				ScrollItD();
				break;
			default:
				break;
		}
	}
	//'左滚动
	function ScrollItL()
	{
		if (psMoveIN){window.setTimeout("ScrollItL();",Delay);return;}
		if (parseInt(document.all("img" + First).style.left)<=(-(ImgWidth+parseInt(ScrollBarWidth/2))) )
		{
			//alert(parseInt(document.all("img" + First).style.left));
			document.all("img" + First).style.left=((ImgWidth+5)*(psgnArrLen-1) + 5 - parseInt(ScrollBarWidth/2)) + "px";
			First=First+1;
			if (First>=psgnArrLen) First=0;
		}
		
		for (i=0;i<psgnArrLen;i++)
		{
			 document.all("img" + i).style.left = (parseInt(document.all("img" + i).style.left) - DX) + "px";
		}
		window.setTimeout("ScrollItL();",Delay);
	}
	//'右滚动
	function ScrollItR()
	{
		if (psMoveIN){window.setTimeout("ScrollItR();",Delay);return;}
		if (parseInt(document.all("img" + First).style.left)>=(ScrollBarWidth/2) )
		{
			document.all("img" + First).style.left=(-((ImgWidth+5)*(psgnArrLen)+5) + ScrollBarWidth/2 + 5) + "px";
			First=First+1;
			if (First>=psgnArrLen) First=0;
		}
		
		for (i=0;i<psgnArrLen;i++)
		{
			 document.all("img" + i).style.left = (parseInt(document.all("img" + i).style.left) + DX) + "px";
		}
		window.setTimeout("ScrollItR();",Delay);
	}
	//'上滚动
	function ScrollItU()
	{
		if (psMoveIN){window.setTimeout("ScrollItU();",Delay);return;}
		if (parseInt(document.all("img" + First).style.top)<=(-(ImgHeight)) )
		{
			document.all("img" + First).style.top=((ImgHeight+5)*(psgnArrLen-1) + 5) + "px";
			First=First+1;
			if (First>=psgnArrLen) First=0;
		}
		
		for (i=0;i<psgnArrLen;i++)
		{
			 document.all("img" + i).style.top = (parseInt(document.all("img" + i).style.top) - DX) + "px";
		}
		window.setTimeout("ScrollItU();",Delay);
	}
	//'下滚动
	function ScrollItD()
	{
		if (psMoveIN){window.setTimeout("ScrollItD();",Delay);return;}
		if (parseInt(document.all("img" + First).style.top)>=(ScrollBarHeight) )
		{
			document.all("img" + First).style.top=(-((ImgHeight+5)*(psgnArrLen)+5) + ScrollBarHeight + 5) + "px";
			First=First+1;
			if (First>=psgnArrLen) First=0;
		}

		for (i=0;i<psgnArrLen;i++)
		{
			 document.all("img" + i).style.top = (parseInt(document.all("img" + i).style.top) + DX) + "px";
		}
		window.setTimeout("ScrollItD();",Delay);
	}
	//设置停止
	function setMoveFlash(fbIn)
	{
		if (psgStop==false){psMoveIN=false;return;}
		if (fbIn) psMoveIN=true;else psMoveIN=false;
	}
	//打印滚动条代码
	function priImageDiv()
	{
		window.document.write("<div align=\"center\" valign=\"middle\" id=\"tk\" Language=\"JavaScript\" onMouseOver=\"setMoveFlash(true);\" onMouseOut=\"setMoveFlash(false);\" style=\"Z-INDEX: 1; LEFT: 278px; OVERFLOW: hidden; WIDTH: 600px; CLIP: rect(0px 100% 110px 0px); TOP: 522px; HEIGHT: 100px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible\"><div align=\"center\" valign=\"middle\" id=\"pssl1\" style=\"POSITION: absolute; WIDTH: 615px; height: 100px; visibility: visible;cursor:hand\"></div></div>");
		window.document.write("<DIV id=\"TipLayer\" style=\"visibility:hidden;position:absolute;z-index:1000;top:-100\"></DIV>")
	}
//结束 图片滚动 代码 By PS

//============以下是Tip代码====================
<!-- 
/*
 Pleas leave this notice.
 DHTML tip message version 1.2 copyright Essam Gamal 2003 (http://migoicons.tripod.com, migoicons@hotmail.com)
 All modifications are done in the style.js you should not modify this file.  Created on : 06/03/2003
 Script featured on and can be found at Dynamic Drive (http://www.dynamicdrive.com)
*/ 

var ua = navigator.userAgent
var ps = navigator.productSub 
var dom = (document.getElementById)? 1:0
var ie4 = (document.all&&!dom)? 1:0
var ie5 = (document.all&&dom)? 1:0
var nn4 =(navigator.appName.toLowerCase() == "netscape" && parseInt(navigator.appVersion) == 4)
var nn6 = (dom&&!ie5)? 1:0
var sNav = (nn4||nn6||ie4||ie5)? 1:0
var cssFilters = ((ua.indexOf("MSIE 5.5")>=0||ua.indexOf("MSIE 6")>=0)&&ua.indexOf("Opera")<0)? 1:0
var Style=[],Count=0,sbw=0,move=0,hs="",mx,my,scl,sct,ww,wh,obj,sl,st,ih,iw,vl,hl,sv,evlh,evlw,tbody
var HideTip = "eval(obj+sv+hl+';'+obj+sl+'=0;'+obj+st+'=-800')"
var doc_root = ((ie5&&ua.indexOf("Opera")<0||ie4)&&document.compatMode=="CSS1Compat")? "document.documentElement":"document.body"
var PX = (nn6)? "px" :"" 

if(sNav) {
	window.onresize = ReloadTip
	document.onmousemove = MoveTip
	if(nn4) document.captureEvents(Event.MOUSEMOVE) 
}	
if(nn4||nn6) {
	mx = "e.pageX"
	my = "e.pageY"
	scl = "window.pageXOffset"
	sct = "window.pageYOffset"	
	if(nn4) {
		obj = "document.TipLayer."
		sl = "left"
		st = "top"
		ih = "clip.height"
		iw = "clip.width"
		vl = "'show'"
		hl = "'hide'"
		sv = "visibility="
	}
	else obj = "document.getElementById('TipLayer')."
} 
if(ie4||ie5) {
	obj = "TipLayer."
	mx = "event.x"
	my = "event.y"
	scl = "eval(doc_root).scrollLeft"
	sct = "eval(doc_root).scrollTop"
	if(ie5) {
		mx = mx+"+"+scl 
		my = my+"+"+sct
	}
}
if(ie4||dom){
	sl = "style.left"
	st = "style.top"
	ih = "offsetHeight"
	iw = "offsetWidth"
	vl = "'visible'"
	hl = "'hidden'"
	sv = "style.visibility="
}
if(ie4||ie5||ps>=20020823) {
	ww = "eval(doc_root).clientWidth"
	wh = "eval(doc_root).clientHeight"
}	 
else { 
	ww = "window.innerWidth"
	wh = "window.innerHeight"
	evlh = eval(wh)
	evlw = eval(ww)
	sbw=15
}	

function applyCssFilter(){
	if(cssFilters&&FiltersEnabled) { 
		var dx = " progid:DXImageTransform.Microsoft."
		TipLayer.style.filter = "revealTrans()"+dx+"Fade(Overlap=1.00 enabled=0)"+dx+"Inset(enabled=0)"+dx+"Iris(irisstyle=PLUS,motion=in enabled=0)"+dx+"Iris(irisstyle=PLUS,motion=out enabled=0)"+dx+"Iris(irisstyle=DIAMOND,motion=in enabled=0)"+dx+"Iris(irisstyle=DIAMOND,motion=out enabled=0)"+dx+"Iris(irisstyle=CROSS,motion=in enabled=0)"+dx+"Iris(irisstyle=CROSS,motion=out enabled=0)"+dx+"Iris(irisstyle=STAR,motion=in enabled=0)"+dx+"Iris(irisstyle=STAR,motion=out enabled=0)"+dx+"RadialWipe(wipestyle=CLOCK enabled=0)"+dx+"RadialWipe(wipestyle=WEDGE enabled=0)"+dx+"RadialWipe(wipestyle=RADIAL enabled=0)"+dx+"Pixelate(MaxSquare=35,enabled=0)"+dx+"Slide(slidestyle=HIDE,Bands=25 enabled=0)"+dx+"Slide(slidestyle=PUSH,Bands=25 enabled=0)"+dx+"Slide(slidestyle=SWAP,Bands=25 enabled=0)"+dx+"Spiral(GridSizeX=16,GridSizeY=16 enabled=0)"+dx+"Stretch(stretchstyle=HIDE enabled=0)"+dx+"Stretch(stretchstyle=PUSH enabled=0)"+dx+"Stretch(stretchstyle=SPIN enabled=0)"+dx+"Wheel(spokes=16 enabled=0)"+dx+"GradientWipe(GradientSize=1.00,wipestyle=0,motion=forward enabled=0)"+dx+"GradientWipe(GradientSize=1.00,wipestyle=0,motion=reverse enabled=0)"+dx+"GradientWipe(GradientSize=1.00,wipestyle=1,motion=forward enabled=0)"+dx+"GradientWipe(GradientSize=1.00,wipestyle=1,motion=reverse enabled=0)"+dx+"Zigzag(GridSizeX=8,GridSizeY=8 enabled=0)"+dx+"Alpha(enabled=0)"+dx+"Dropshadow(OffX=3,OffY=3,Positive=true,enabled=0)"+dx+"Shadow(strength=3,direction=135,enabled=0)"
	}
}

function stm(t,s) {
  if(sNav) {
  	if(t.length<2||s.length<25) {
		var ErrorNotice = "DHTML TIP MESSAGE VERSION 1.2 ERROR NOTICE.\n"
		if(t.length<2&&s.length<25) alert(ErrorNotice+"It looks like you removed an entry or more from the Style Array and Text Array of this tip.\nTheir should be 25 entries in every Style Array even though empty and 2 in every Text Array. You defined only "+s.length+" entries in the Style Array and "+t.length+" entry in the Text Array. This tip won't be viewed to avoid errors")
		else if(t.length<2) alert(ErrorNotice+"It looks like you removed an entry or more from the Text Array of this tip.\nTheir should be 2 entries in every Text Array. You defined only "+t.length+" entry. This tip won't be viewed to avoid errors.")
		else if(s.length<25) alert(ErrorNotice+"It looks like you removed an entry or more from the Style Array of this tip.\nTheir should be 25 entries in every Style Array even though empty. You defined only "+s.length+" entries. This tip won't be viewed to avoid errors.")
 	}
  	else {
		var ab = "" ;var ap = ""
		var titCol = (s[0])? "COLOR='"+s[0]+"'" : ""
		var txtCol = (s[1])? "COLOR='"+s[1]+"'" : ""
		var titBgCol = (s[2])? "BGCOLOR='"+s[2]+"'" : ""
		var txtBgCol = (s[3])? "BGCOLOR='"+s[3]+"'" : ""
		var titBgImg = (s[4])? "BACKGROUND='"+s[4]+"'" : ""	
		var txtBgImg = (s[5])? "BACKGROUND='"+s[5]+"'" : ""
		var titTxtAli = (s[6] && s[6].toLowerCase()!="left")? "ALIGN='"+s[6]+"'" : ""
		var txtTxtAli = (s[7] && s[7].toLowerCase()!="left")? "ALIGN='"+s[7]+"'" : ""   
		var add_height = (s[15])? "HEIGHT='"+s[15]+"'" : ""
		if(!s[8])  s[8] = "Verdana,Arial,Helvetica"
		if(!s[9])  s[9] = "Verdana,Arial,Helvetica"					
		if(!s[12]) s[12] = 1
		if(!s[13]) s[13] = 1
		if(!s[14]) s[14] = 200
		if(!s[16]) s[16] = 0
		if(!s[17]) s[17] = 0
		if(!s[18]) s[18] = 10
		if(!s[19]) s[19] = 10
		hs = s[11].toLowerCase() 
		if(ps==20001108){
		if(s[2]) ab="STYLE='border:"+s[16]+"px solid"+" "+s[2]+"'"
		ap="STYLE='padding:"+s[17]+"px "+s[17]+"px "+s[17]+"px "+s[17]+"px'"}
		var closeLink=(hs=="sticky")? "<TD ALIGN='right'><FONT SIZE='"+s[12]+"' FACE='"+s[8]+"'><A HREF='javascript:void(0)' ONCLICK='stickyhide()' STYLE='text-decoration:none;color:"+s[0]+"'><B>Close</B></A></FONT></TD>":""
		var title=(t[0]||hs=="sticky")? "<TABLE WIDTH='100%' BORDER='0' CELLPADDING='0' CELLSPACING='0'><TR><TD "+titTxtAli+"><FONT SIZE='"+s[12]+"' FACE='"+s[8]+"' "+titCol+"><B>"+t[0]+"</B></FONT></TD>"+closeLink+"</TR></TABLE>" : ""
		var txt="<TABLE "+titBgImg+" "+ab+" WIDTH='"+s[14]+"' BORDER='0' CELLPADDING='"+s[16]+"' CELLSPACING='0' "+titBgCol+" ><TR><TD>"+title+"<TABLE WIDTH='100%' "+add_height+" BORDER='0' CELLPADDING='"+s[17]+"' CELLSPACING='0' "+txtBgCol+" "+txtBgImg+"><TR><TD "+txtTxtAli+" "+ap+" VALIGN='top'><FONT SIZE='"+s[13]+"' FACE='"+s[9]+"' "+txtCol +">"+t[1]+"</FONT></TD></TR></TABLE></TD></TR></TABLE>"
		if(nn4) {
			with(eval(obj+"document")) {
				open()
				write(txt)
				close()
			}
		}
		else eval(obj+"innerHTML=txt")
		tbody = {
			Pos:s[10].toLowerCase(), 
			Xpos:s[18],
			Ypos:s[19], 
			Transition:s[20],
			Duration:s[21], 
			Alpha:s[22],
			ShadowType:s[23].toLowerCase(),
			ShadowColor:s[24],
			Width:parseInt(eval(obj+iw)+3+sbw)
		}
		if(ie4) { 
			TipLayer.style.width = s[14]
	 		tbody.Width = s[14]
		}
		Count=0	
		move=1
 	 }
  }
}

function MoveTip(e) {
	if(move) {
		var X,Y,MouseX = eval(mx),MouseY = eval(my); tbody.Height = parseInt(eval(obj+ih)+3)
		tbody.wiw = parseInt(eval(ww+"+"+scl)); tbody.wih = parseInt(eval(wh+"+"+sct))
		switch(tbody.Pos) {
			case "left" : X=MouseX-tbody.Width-tbody.Xpos; Y=MouseY+tbody.Ypos; break
			case "center": X=MouseX-(tbody.Width/2); Y=MouseY+tbody.Ypos; break
			case "float": X=tbody.Xpos+eval(scl); Y=tbody.Ypos+eval(sct); break	
			case "fixed": X=tbody.Xpos; Y=tbody.Ypos; break		
			default: X=MouseX+tbody.Xpos; Y=MouseY+tbody.Ypos
		}

		if(tbody.wiw<tbody.Width+X) X = tbody.wiw-tbody.Width
		if(tbody.wih<tbody.Height+Y+sbw) {
			if(tbody.Pos=="float"||tbody.Pos=="fixed") Y = tbody.wih-tbody.Height-sbw
			else Y = MouseY-tbody.Height
		}
		if(X<0) X=0 
		eval(obj+sl+"=X+PX;"+obj+st+"=Y+PX")
		ViewTip()
	}
}

function ViewTip() {
  	Count++
	if(Count == 1) {
		if(cssFilters&&FiltersEnabled) {	
			for(Index=28; Index<31; Index++) { TipLayer.filters[Index].enabled = 0 }
			for(s=0; s<28; s++) { if(TipLayer.filters[s].status == 2) TipLayer.filters[s].stop() }
			if(tbody.Transition == 51) tbody.Transition = parseInt(Math.random()*50)
			var applyTrans = (tbody.Transition>-1&&tbody.Transition<24&&tbody.Duration>0)? 1:0
			var advFilters = (tbody.Transition>23&&tbody.Transition<51&&tbody.Duration>0)? 1:0
			var which = (applyTrans)?0:(advFilters)? tbody.Transition-23:0 
			if(tbody.Alpha>0&&tbody.Alpha<100) {
	  			TipLayer.filters[28].enabled = 1
	  			TipLayer.filters[28].opacity = tbody.Alpha
			}
			if(tbody.ShadowColor&&tbody.ShadowType == "simple") {
	  			TipLayer.filters[29].enabled = 1
	  			TipLayer.filters[29].color = tbody.ShadowColor
			}
			else if(tbody.ShadowColor&&tbody.ShadowType == "complex") {
	  			TipLayer.filters[30].enabled = 1
	  			TipLayer.filters[30].color = tbody.ShadowColor
			}
			if(applyTrans||advFilters) {
				eval(obj+sv+hl)
	  			if(applyTrans) TipLayer.filters[0].transition = tbody.Transition
	  			TipLayer.filters[which].duration = tbody.Duration 
	  			TipLayer.filters[which].apply()
			}
		}
 		eval(obj+sv+vl)
		if(cssFilters&&FiltersEnabled&&(applyTrans||advFilters)) TipLayer.filters[which].play()
		if(hs == "sticky") move=0
  	}
}

function stickyhide() {
	eval(HideTip)
}

function ReloadTip() {
	 if(nn4&&(evlw!=eval(ww)||evlh!=eval(wh))) location.reload()
	 else if(hs == "sticky") eval(HideTip)
}

function htm() {
	if(sNav) {
		if(hs!="keep") {
			move=0; 
			if(hs!="sticky") eval(HideTip)
		}	
	} 
}

var FiltersEnabled = 1 // if your not going to use transitions or filters in any of the tips set this to 0

Style[0]=["white","black","#000099","#E8E8FF","","","","","","","","","","",200,"",2,2,10,10,51,1,0,"",""]
Style[1]=["white","black","#000099","#E8E8FF","","","","","","","center","","","",200,"",2,2,10,10,"","","","",""]
Style[2]=["white","black","#000099","#E8E8FF","","","","","","","left","","","",200,"",2,2,10,10,"","","","",""]
Style[3]=["white","black","#000099","#E8E8FF","","","","","","","float","","","",200,"",2,2,10,10,"","","","",""]
Style[4]=["white","black","#000099","#E8E8FF","","","","","","","fixed","","","",200,"",2,2,1,1,"","","","",""]
Style[5]=["white","black","#000099","#E8E8FF","","","","","","","","sticky","","",200,"",2,2,10,10,"","","","",""]
Style[6]=["white","black","#000099","#E8E8FF","","","","","","","","keep","","",200,"",2,2,10,10,"","","","",""]
Style[7]=["white","black","#000099","#E8E8FF","","","","","","","","",2,3,200,"",2,2,40,10,"","","","",""]
Style[8]=["white","black","#000099","#E8E8FF","","","","","","","","","","",200,"",2,2,20,50,"","","","",""]
Style[9]=["white","black","#000099","#E8E8FF","","","","","","","","","","",200,"",2,2,10,10,51,0.5,75,"simple","gray"]
Style[10]=["white","black","black","white","","","right","","Impact","cursive","center","",3,5,200,150,5,20,10,0,50,1,80,"complex","gray"]
Style[11]=["white","black","#000099","#E8E8FF","","","","","","","","","","",200,"",2,2,10,10,51,0.5,45,"simple","gray"]
Style[12]=["white","black","#000099","#E8E8FF","","","","","","","","","","",200,"",2,2,10,10,"","","","",""]

//-->
