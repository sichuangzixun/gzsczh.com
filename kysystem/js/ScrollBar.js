//Build By P.S 2005-08-06(2005-11-14) winpzs@hotmail.com v0.2
/*===========================================================================
用法1:
	<Script Language="JavaScript" Src="./scrollbar.js"></Script>
	<Script Language="JavaScript">startImageScroll(1);</Script>
	<!--在这里放(HTML)滚动内容,滚动内容的width要比滚动条长度要长--><table width=700><tr><td nowrap>你好</td></tr></table>
	<Script Language="JavaScript">endImageScroll(1,30,2,4,"600px","120px",0,0);</Script>

用法2:
	<Script Language="JavaScript" Src="./scrollbar.js"></Script>
	<Script Language="JavaScript">psScrollBarHTML(1,30,2,4,"700px","120px",0,0,"<table width=700><tr><td nowrap>你好</td></tr></table>");</Script>

注意:滚动内容的width要比滚动条长度要长
	如:
	滚动内容:<table width=700><tr><td nowrap>你好</td></tr></table>
	滚动条长度要小于700
===========================================================================*/
//用支持每页面多条滚动条
var gImageAutoID = -1;	//自动ID
var gImageAutoCls=[];	//clsImageScorllBar类,数组

	//strID		滚动条ID(可以每页面多条滚动条)
	//pSpeed	速度数值越大速度越慢(微秒)
	//pStep		步数数值越大速度越快
	//pRunType	滚动方向(1为上,2为下,3为左,4为右)
	//pWidth	总宽度
	//pHeight	总高度
	//pperScroll流动多少就暂停一次
	//ppsc		暂停多少时间(微秒)
	function clsImageScorllBar(strID,pSpeed,pStep,pRunType,pWidth,pHeight,pperScroll,ppsc)
	{
		this.clsID = strID;
		this.g_ImageSpeed= pSpeed;
		this.g_ImageStep = pStep;//步数数值越大速度越快
		this.g_ImageRunType = pRunType;//滚动方向(1为上,2为下,3为左,4为右)
		this.g_ImageAllWidth = pWidth;//滚动方向(1为上,2为下,3为左,4为右)
		this.g_ImageAllHeight = pHeight;//滚动方向(1为上,2为下,3为左,4为右)

		this.g_ImagePerScroll = pperScroll;//流动多少就暂停一次
		this.g_Imagepsc = ppsc;//暂停多少时间(秒)
		this.g_ImageScrollCount = 0;//流动计数

		this.bMouseOver = false;

		this.ps_ImageScroll_demo = eval("document.all.ps_ImageScroll_demo_h" + this.clsID);
		this.ps_ImageScroll_demo1 = eval("document.all.ps_ImageScroll_demo1_h" + this.clsID);
		this.ps_ImageScroll_demo2 = eval("document.all.ps_ImageScroll_demo2_h" + this.clsID);

		this.ps_ImageScroll_demo.style.width = this.g_ImageAllWidth;
		this.ps_ImageScroll_demo.style.height = this.g_ImageAllHeight;
		this.ps_ImageScroll_demo2.innerHTML=this.ps_ImageScroll_demo1.innerHTML;
	}


	function ps_ImageScroll_Marquee(fImageScroll)
	{
		if(typeof(fImageScroll)!="object")return;
		if (fImageScroll.bMouseOver)
		{
			window.setTimeout("ps_ImageScroll_Marquee(gImageAutoCls["+fImageScroll.clsID+"]);",fImageScroll.g_ImageSpeed);
			return;
		}
		switch(fImageScroll.g_ImageRunType)
		{
		///*左滚动
		case 3:
			if(fImageScroll.ps_ImageScroll_demo1.offsetWidth-fImageScroll.ps_ImageScroll_demo.scrollLeft<=0)
				fImageScroll.ps_ImageScroll_demo.scrollLeft -= fImageScroll.ps_ImageScroll_demo1.offsetWidth;
			else
				fImageScroll.ps_ImageScroll_demo.scrollLeft += fImageScroll.g_ImageStep;
			break;
		//*/
		///*右滚动
		case 4:
			if (fImageScroll.ps_ImageScroll_demo.scrollLeft <= 0)
				fImageScroll.ps_ImageScroll_demo.scrollLeft += fImageScroll.ps_ImageScroll_demo1.offsetWidth;
			else
				fImageScroll.ps_ImageScroll_demo.scrollLeft -= fImageScroll.g_ImageStep;
			break;
		//*/
		///*下滚动
		case 2:
			if (fImageScroll.ps_ImageScroll_demo.scrollTop <= 0)
				fImageScroll.ps_ImageScroll_demo.scrollTop += fImageScroll.ps_ImageScroll_demo1.offsetHeight;
			else
				fImageScroll.ps_ImageScroll_demo.scrollTop -= fImageScroll.g_ImageStep;
			break;
		//*/
		///*上滚动
		case 1:
			if (fImageScroll.ps_ImageScroll_demo1.offsetHeight-fImageScroll.ps_ImageScroll_demo.scrollTop <= 0)
				fImageScroll.ps_ImageScroll_demo.scrollTop -= fImageScroll.ps_ImageScroll_demo1.offsetHeight;
			else
				fImageScroll.ps_ImageScroll_demo.scrollTop += fImageScroll.g_ImageStep;
			break;
		default:
			break;
		//*/
		}
		if (fImageScroll.g_ImagePerScroll != 0)
		{
			fImageScroll.g_ImageScrollCount += fImageScroll.g_ImageStep;
			if (fImageScroll.g_ImageScrollCount >= fImageScroll.g_ImagePerScroll)
			{
				window.setTimeout("ps_ImageScroll_Marquee(gImageAutoCls["+fImageScroll.clsID+"]);",fImageScroll.g_Imagepsc);
				fImageScroll.g_ImageScrollCount = 0;
				return;
			}
		}
		window.setTimeout("ps_ImageScroll_Marquee(gImageAutoCls["+fImageScroll.clsID+"]);",fImageScroll.g_ImageSpeed);

	}


	function startImageScroll()
	{
		if (gImageAutoID<-1) gImageAutoID = -1;
		gImageAutoID++;

		var strID;
		strID = gImageAutoID;
		window.document.write("<div id=ps_ImageScroll_demo_h" + strID + " align=\"center\" valign=\"middle\" style=\"overflow:hidden;height:120;width:500;margin-top:4px;\"><table><tr><td id=ps_ImageScroll_demo1_h" + strID + ">");
	}

	//pSpeed	速度数值越大速度越慢(微秒)
	//pStep		步数数值越大速度越快
	//pRunType	滚动方向(1为上,2为下,3为左,4为右)
	//pWidth	总宽度
	//pHeight	总高度
	//pperScroll流动多少就暂停一次
	//ppsc		暂停多少时间(微秒)
	function endImageScroll(pSpeed,pStep,pRunType,pWidth,pHeight,pperScroll,ppsc)
	{
		if (gImageAutoID<0) gImageAutoID = 0;

		var strID;
		strID = gImageAutoID;
		if (pRunType <= 2)
			window.document.write("</td></tr><tr><td id=ps_ImageScroll_demo2_h" + strID + "></td></tr></table></div>");
		else
			window.document.write("</td><td id=ps_ImageScroll_demo2_h" + strID + "></td></tr></table></div>");
		gImageAutoCls[strID] = new clsImageScorllBar(strID,pSpeed,pStep,pRunType,pWidth,pHeight,pperScroll,ppsc);

		window.setTimeout("ps_ImageScroll_Marquee(gImageAutoCls["+strID+"]);",gImageAutoCls[strID].g_ImageSpeed);
		if (gImageAutoCls[strID].g_ImagePerScroll == 0)
		{
			gImageAutoCls[strID].ps_ImageScroll_demo.onmouseover=function() {gImageAutoCls[strID].bMouseOver=true;}
			gImageAutoCls[strID].ps_ImageScroll_demo.onmouseout=function() {gImageAutoCls[strID].bMouseOver=false;}
		}

	}

	//pSpeed	速度数值越大速度越慢(微秒)
	//pStep		步数数值越大速度越快
	//pRunType	滚动方向(1为上,2为下,3为左,4为右)
	//pWidth	总宽度
	//pperScroll流动多少就暂停一次
	//ppsc		暂停多少时间(微秒)
	//strInnerHTML		滚动条的内容(用于设计时不变形,但要转换字串)
	function psScrollBarHTML(pSpeed,pStep,pRunType,pWidth,pHeight,pperScroll,ppsc,strInnerHTML)
	{
		startImageScroll();
		window.document.write(strInnerHTML);
		endImageScroll(pSpeed,pStep,pRunType,pWidth,pHeight,pperScroll,ppsc)
	}
