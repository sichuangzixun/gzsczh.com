/*
Build By P.S. 2005-11-30
*/

function PS_clsFilter()
{
	this.mName = "";	//定义该类变量名称
	this.mTimer = 3000;			//转换时间;
	this.mWidth = "100%";		//宽度
	this.mHeight = "100%";		//高度
	this.mTitle = new Array();	//内容数组
	this.mTransition = 23;		//变化的样式
	this.mCount = 0;	//数组维数(内部用)
	this.mNum = 0;		//当前页面(内部用)
	this.mCurTime = null;	//当前时间进程指针(内部用)
	this.mhtmlObj = null;//div对象(内部用)

	this.isOK = function()
	{
		if (typeof(this.mhtmlObj)!="object") return false;
		if (this.mCount<=0) return false;
		if (this.mName=="") return false;
		return true;
	}

	this.PlayNext = function()
	{
		if (!this.isOK()) return;
		if(this.mNum<this.mCount-1) this.mNum++ ;else this.mNum=0;
		this.mhtmlObj.filters.revealTrans.Transition=this.mTransition;
		this.mhtmlObj.filters.revealTrans.apply();
		this.mhtmlObj.innerHTML=this.mTitle[this.mNum];
		this.mhtmlObj.filters.revealTrans.play();
		this.mCurTime = window.setTimeout(this.mName + ".PlayNext()",this.mTimer);
	}
	this.Stop = function()
	{
		if (!this.isOK()) return;
		window.clearTimeout(this.mCurTime);
	}
	this.Start = function()
	{
		if (!this.isOK()) return;
		this.mCurTime = window.setTimeout(this.mName + ".PlayNext()",this.mTimer);
	}

	this.toString = function()
	{
		if (this.mCount<=0) return "";
		return "<div id=\"ps_Filter_DivName_"+this.mName+"\" STYLE=\"FILTER:revealTrans(duration=2, transition=3); VISIBILITY:visible; WIDTH:"+this.mWidth+"; POSITION:relative; HEIGHT:"+this.mHeight+";\" onmouseover=\""+this.mName+".Stop();\" onmouseout=\""+this.mName+".Start()\">"+this.mTitle[0]+"</div>";
	}

	this.Play = function()
	{
		if (this.mhtmlObj!=null) return;
		this.mCount = this.mTitle.length;
		if (this.mCount<=0) return;
		window.document.write(this);
		this.mhtmlObj = eval("document.all.ps_Filter_DivName_"+this.mName);
		this.PlayNext();
	}
}
/*
var PS_g_Test = new PS_clsFilter();
PS_g_Test.mName = "PS_g_Test";	//一定要跟变量名字相同
PS_g_Test.mWidth = "160px";
PS_g_Test.mHeight = "120px";
PS_g_Test.mTransition = 21;
PS_g_Test.mTimer = 5000;
PS_g_Test.mTitle[0]='<table cellpadding=0 align=center width=100% cellspacing=0><tr bgcolor=#F2F5F7><td width=14 height=25 align=center valign=middle>·</td><td width=65><a href=/search.html?p=五福娃&source=ysearch_news_hp_key class=move target="_blank">五福娃</a></td><td width=14 align=center valign=middle>·</td><td width=65> <a href=/search.html?p=%C7%DD%C1%F7%B8%D0&source=ysearch_news_hp_key class=move target="_blank">禽流感</a></td></tr><tr bgcolor=#ffffff><td height=25 align=center valign=middle>·</td><td > <a href=/search.html?p=哈里+波特&source=ysearch_news_hp_key class=move target="_blank">哈里.波特</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=养老保险&source=ysearch_news_hp_key class=move target="_blank">养老保险</a></td></tr><tr bgcolor=#F2F5F7><td align=center valign=middle>·</td><td height=25> <a href=/search.html?p=公务员考试&source=ysearch_news_hp_key class=move target="_blank">公务员考试</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=李宇春&source=ysearch_news_hp_key class=move target="_blank">李宇春</a></td></tr><tr bgcolor=#ffffff><td align=center valign=middle>·</td><td height=25> <a href=/search.html?p=%B4%F3%B3%A4%BD%F1&source=ysearch_news_hp_key class=move target="_blank">大长今</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=王菲怀孕&source=ysearch_news_hp_key class=move target="_blank">王菲怀孕</a></td></tr><tr bgcolor=#F2F5F7><td align=center valign=middle>·</td><td height=25> <a href=/search.html?p=楼市&source=ysearch_news_hp_key class=move target="_blank">楼市</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=哈尔滨停水&source=ysearch_news_hp_key class=move target="_blank">哈尔滨停水</a></td></tr></table>';
PS_g_Test.mTitle[1]='<table cellpadding=0 align=center width=100% cellspacing=0><tr bgcolor=#F2F5F7><td width=14 height=25 align=center valign=middle>·</td><td width=65><a href=/search.html?p=校园招聘&source=ysearch_news_hp_key class=move target="_blank">校园招聘</a></td><td width=14 align=center valign=middle>·</td><td width=65> <a href=/search.html?p=%B6%CC%D0%C5%B7%B8%D7%EF&source=ysearch_news_hp_key class=move target="_blank">短信犯罪</a></td></tr><tr bgcolor=#ffffff><td height=25 align=center valign=middle>·</td><td > <a href=/search.html?p=张靓颖&source=ysearch_news_hp_key class=move target="_blank">张靓颖</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=%B3%B5%BC%DB%D7%DF%CA%C6&source=ysearch_news_hp_key class=move target="_blank">车价走势</a></td></tr><tr bgcolor=#F2F5F7><td align=center valign=middle>·</td><td height=25> <a href=/search.html?p=%B0%C2%D4%CB&source=ysearch_news_hp_key class=move target="_blank">奥运</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=NBA&source=ysearch_news_hp_key class=move target="_blank">NBA</a></td></tr><tr bgcolor=#ffffff><td align=center valign=middle>·</td><td height=25> <a href=/search.html?p=夜宴&source=ysearch_news_hp_key class=move target="_blank">夜宴</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=股改&source=ysearch_news_hp_key class=move target="_blank">股改</a></td></tr><tr bgcolor=#F2F5F7><td align=center valign=middle>·</td><td height=25> <a href=/search.html?p=%D6%DC%BD%DC%C2%D7&source=ysearch_news_hp_key class=move target="_blank">周杰伦</a></td><td align=center valign=middle>·</td><td> <a href=/search.html?p=超女&source=ysearch_news_hp_key class=move target="_blank">超女</a></td></tr></table>';
PS_g_Test.Play();
*/