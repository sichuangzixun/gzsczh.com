function selall(objname,act){
	if(objname==null)
		return false;
	else if(objname.length==null)
		objname.checked=act;
	else
	{
		var n=objname.length;
		for (i=0;i<n;i++)
		{
			objname[i].checked=act;
		}
	}
}
function formsubmit(formname,checkname,act,bBox)
{
	formname.action=act;
//	alert(typeof(bBox)=="undefined")
	//alert(formname.action);
	if (chkSelect(checkname)==true)
	{
		if (bBox == false)
		{
			formname.submit();
		}
		else if(confirm("你真的要执行本次操作？"))
		{
			formname.submit();
		}
	}
	else
	{
		alert("你还没有选择任何用户！");
	}
}

function chkSelect(checkname)
{
	if (typeof(checkname)!="object") return false;
	var bRet = false;
	if(checkname==null||(checkname.length==null&&!checkname.checked))
	{
		bRet = false;
	}
	else if(checkname.length==null&&checkname.checked)
	{
		bRet = true;
	}
	else
	{
		var issel=false;
		var n=checkname.length;
		for (i=0;i<n;i++)
		{
			if(checkname[i].checked)
			{
				issel=true;
				break;
			}
		}
		if(issel)
		{
			bRet = true;
		}
		else
		{
			bRet = false;
		}
	}
	return bRet;
}