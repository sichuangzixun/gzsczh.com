<?php /* Smarty version Smarty-3.1.17, created on 2018-10-24 09:54:41
         compiled from "D:\wamp\www\gzsczx.com.cn\templates\zixun\case.html" */ ?>
<?php /*%%SmartyHeaderCode:268645bcfd0e18737c0-43077975%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '543ddc07c4e168399336d959b11e1778587493ff' => 
    array (
      0 => 'D:\\wamp\\www\\gzsczx.com.cn\\templates\\zixun\\case.html',
      1 => 1540342311,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '268645bcfd0e18737c0-43077975',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_title' => 0,
    'keywords' => 0,
    'description' => 0,
    'listB' => 0,
    'datas' => 0,
    'allPage' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_5bcfd0e1922464_04699680',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bcfd0e1922464_04699680')) {function content_5bcfd0e1922464_04699680($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>经典案例_<?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
</title>
<meta name="Keywords" content="<?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
" />
<meta name="Description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
" />
 <?php  $_config = new Smarty_Internal_Config("./config/config.conf", $_smarty_tpl->smarty, $_smarty_tpl);$_config->loadConfigVars(null, 'local'); ?>
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
css/reset.css" type="text/css" media="all" />
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
css/base_acg.css" type="text/css" media="all" />
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
css/base_acg1.css" type="text/css" media="all" />
 
  <script type="text/javascript" src="js/common.js"></script>
 
</head>


<body>


<?php echo $_smarty_tpl->getSubTemplate ("top.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>







<!-- bg -->  

<!-- search -->
<div class="cont3 clearfix">
   <!--ur here-->
   <p class="news bnav about left"><a href="./" target="_blank">首页</a> &gt; <a href="case.php">经典案例</a> >  列表</p>
   
   <?php echo $_smarty_tpl->getSubTemplate ("search_right.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div> 
<!-- line -->
<div id="line">
  <div class="w-line"><div class="line-con1"></div></div>
  <div class="line-right"></div>
</div>
<!-- content -->
<div id="acg-content">
 <div class="mainbox clearfix">
  <div class="menunav left">
    <ul class="clearfix">

    <!--当前栏目可用类别 start-->
	<?php echo $_smarty_tpl->tpl_vars['listB']->value;?>

       
    	           
    	        	        	        	 
    
    </ul>
  </div>
  <div class="menuravline left"></div>
  <div class="contbox padbm left">
     <div class="mainconbox">       
        <div class="conbox-cont">
           <div class="conbox-cont-main left">
             <div class="conbox-cont-main1">
			  <!--当前栏目所选类别的文章列|truncate:260表 start-->
			  
			   <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['datas']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>
			   <div class="contbox-main1 contbox-main3">
                 <div class="contbox-main3pic left">   
          			<a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
" target="_self"><img src="<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['pImage'];?>
" width="245" height="146" border="0" alt=""/></a>
                 </div>
                 <dl>
                   <dt><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
" target="_self"><?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nName'];?>
</a></dt>
                   <dd><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nRemark']);?>
[<a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
" target="_self">详细</a>]</dd>
                 </dl>
              </div>
			  
			   <?php endfor; endif; ?>
                                                                      <!--当前栏目所选类别的文章列表 end-->
     
            </div>


 <!--分页 start-->
            <ol class="pagination" id="pages"> 
					<?php echo $_smarty_tpl->tpl_vars['allPage']->value;?>

			
			
			
			
			</ol>
            <!--分页 end-->
             
			 
			 
            
            <div class="contbox-main4">
                <p>
                <!--<a href="#" target="_blank" class="rxbtn">更多客户案例 >></a>-->
                
                
                                                                                
                </p>
            </div>
              

           </div>
           
           
           
           <div class="conbox-cont-side left">
              <?php echo $_smarty_tpl->getSubTemplate ("right_phone.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                      
           </div>
           
           
           
        </div>
    </div>
  </div>
 </div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


</body>
</html>

<?php }} ?>
