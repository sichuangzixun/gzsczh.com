<?php /* Smarty version Smarty-3.1.17, created on 2014-05-11 02:27:07
         compiled from "/www/users/gzsczx.com.cn/templates/zixun/team.html" */ ?>
<?php /*%%SmartyHeaderCode:1357867736536e6f7b8ef8c8-33593761%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '28801c4e9f06d186c43da189624216a6d135cbf9' => 
    array (
      0 => '/www/users/gzsczx.com.cn/templates/zixun/team.html',
      1 => 1399629235,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1357867736536e6f7b8ef8c8-33593761',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_title' => 0,
    'keywords' => 0,
    'description' => 0,
    'listB' => 0,
    'datas' => 0,
    'allPage' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_536e6f7baa68a9_31995036',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_536e6f7baa68a9_31995036')) {function content_536e6f7baa68a9_31995036($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>专家团队_<?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
</title>
<meta name="Keywords" content="<?php echo $_smarty_tpl->tpl_vars['keywords']->value;?>
" />
<meta name="Description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
" />
 <?php  $_config = new Smarty_Internal_Config("./config/config.conf", $_smarty_tpl->smarty, $_smarty_tpl);$_config->loadConfigVars(null, 'local'); ?>
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
css/reset.css" type="text/css" media="all" />
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
css/base_acg.css" type="text/css" media="all" />
 <link rel="stylesheet" href="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
css/base_acg1.css" type="text/css" media="all" />
 
  <script type="text/javascript" src="js/common.js"></script>
</head>


<body>



<?php echo $_smarty_tpl->getSubTemplate ("top.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>







<!-- bg -->  

<!-- search -->
<div class="cont3 clearfix">
   <!--ur here-->
   <p class="news bnav about left"><a href="./" target="_blank">首页</a> &gt; 
   <a href="team.php">专家团队</a> >  正文</p>
  <?php echo $_smarty_tpl->getSubTemplate ("search_right.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div> 
<!-- line -->
<div id="line">
  <div class="w-line"><div class="line-con1"></div></div>
  <div class="line-right"></div>
</div>
<!-- content -->
<div id="acg-content">
 <div class="mainbox clearfix">
	<div class="menunav left">
  
  
    <ul class="clearfix" id="tab6">
    <!--一级栏目"功能咨询"的子栏目 start-->
        <!--一级栏目"功能咨询"的子栏目 end-->
    
    <!--二级栏目及伸缩的三级栏目菜单 start-->
	<?php echo $_smarty_tpl->tpl_vars['listB']->value;?>

                <li class="hand" id="rxal_side15">
    	
          
        <!--二级栏目及伸缩的三级栏目菜单 end-->
    </ul>
    

  </div>
  
  
  <div class="menuravline left"></div>
  <div class="contbox padbm left">
     <div class="mainconbox">
        <ul class="conbox-nav clearfix">
		 
          <li class="hand current" id="acdnav3">专家团队</li>
         
        </ul>
        
       
           
           
      
        
        
        
        
        
        
        
        
      
        
        
        
        
        
        
        
        <!--选项卡"专家团队"的内容 注意：此处列表传参多一个参数&pre_catid才行!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! start-->
        <div class="conbox-cont pdb40" id="acdnav3_con" >
           <div class="conbox-cont-main conbox-cont-main-td"> 
             <div class="conbox-cont-main1">
             
              <div class="contbox-side1 right">
                <a href="javascript:void(0)" title="联系专家" class="acg-email" id="acgemai3">联系专家</a>
                <p class="contbox-side1_call"><img src="<?php echo $_smarty_tpl->getConfigVariable('root_path');?>
images/400.jpg" alt="服务热线" width="160" height="36" /></p>
              </div> 
              
              <ul class="clearfix">
              <!--推荐位：专家团队(“当前栏目名”> 专家团队) start-->
              <!--专家是另一个栏目下的文章，跨栏目调用时，要写源栏目的catid,切记!-->
                            
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['datas']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>		
				 <li>           
                   <div class="contbox-main1 contbox-main3 left">
                     <div class="contbox-main3pic left">
                        <a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
" target="_self"><img src="<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['pImage'];?>
" width="179" height="103" border="0" alt=""/></a>
                     </div>
                     <dl class="acg-zjtd">
                       <dt><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
" target="_self"><?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nName'];?>
</a></dt>
                      <!-- <dd>CEO、合伙人</dd>-->
                       <dd><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nRemark']);?>
[<a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
" target="_self">详细</a>]</dd>
                     </dl>
                   </div>
				   <!--
                   <div class="contbox-side-td right">
                      <h4>个人擅长领域：</h4>
                      <p><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
">战略执行与保障</a><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
">管控模式</a><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
">组织结构设计</a><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['datas']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['nId'];?>
">人力资源管理</a></p>
                   </div>
				   -->
                </li>
                               
					   <?php endfor; endif; ?>		   
							   
							                                     <!--推荐位：专家团队(“当前栏目名”> 专家团队) start-->
                
              </ul>
            </div>
			
			
			
 <!--分页 start-->
            <ol class="pagination" id="pages"> 
					<?php echo $_smarty_tpl->tpl_vars['allPage']->value;?>

			
			
			
			
			</ol>
            <!--分页 end-->
			
			
            <div class="contbox-main4">
                <p><!--<a href="#" target="_blank" class="rxbtn">更多客户案例 >></a>--><!--<a href="#" target="_blank" class="rxbtn">该领域专家介绍 >></a>--></p>
              </div>
           </div>
        </div>
        <!--选项卡"专家团队"的内容 end--> 
        
        
        
        
        
        
        
        
        
      
            
           
      
    </div>
  </div>
 </div>
</div>



<?php echo $_smarty_tpl->getSubTemplate ("footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</body>
</html><?php }} ?>
