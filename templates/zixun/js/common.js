/* *
 * 获得get方式传递的参数值
 */
function GetQueryString(name) {
         var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
         var r = window.location.search.substr(1).match(reg); 
         if (r != null)  return unescape(r[2]);
         else return null; 
}


/* 通过检查User Agent中是否有iPad , 来判断客户端是否为iPad */
function is_iPad(){ 
    var ua = navigator.userAgent.toLowerCase(); 
    if(ua.match(/iPad/i)=="ipad") {
		//alert('是iPad')
		return true; 
    } else {
		//alert('不是iPad')
		return false; 
    } 
}
